import { Component, OnInit } from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe("Test Recipe", "Bacon ipsum dolor amet brisket jowl pig, kielbasa short ribs beef ribs leberkas doner capicola strip steak hamburger. Porchetta sausage rump, biltong ball tip beef ribs t-bone meatball tail pork belly andouille turkey pancetta. Venison meatball porchetta kevin bresaola, tongue cow corned beef pork loin.",
      "https://angular.io/assets/images/logos/angular/angular.png"),
    new Recipe("Test Recipe", "Bacon ipsum dolor amet brisket jowl pig, kielbasa short ribs beef ribs leberkas doner capicola strip steak hamburger. Porchetta sausage rump, biltong ball tip beef ribs t-bone meatball tail pork belly andouille turkey pancetta. Venison meatball porchetta kevin bresaola, tongue cow corned beef pork loin.",
      "https://angular.io/assets/images/logos/angular/angular.png"),
    new Recipe("Test Recipe", "Bacon ipsum dolor amet brisket jowl pig, kielbasa short ribs beef ribs leberkas doner capicola strip steak hamburger. Porchetta sausage rump, biltong ball tip beef ribs t-bone meatball tail pork belly andouille turkey pancetta. Venison meatball porchetta kevin bresaola, tongue cow corned beef pork loin.",
      "https://angular.io/assets/images/logos/angular/angular.png")
  ];

  constructor() { }

  ngOnInit() {
  }

}
