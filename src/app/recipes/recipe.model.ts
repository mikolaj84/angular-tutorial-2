export class Recipe{
  public name: string;
  public descritpion: string;
  public imagePath: string;

  constructor(name: string, descritpion: string, imagePath: string) {
    this.name = name;
    this.descritpion = descritpion;
    this.imagePath = imagePath;
  }
}
